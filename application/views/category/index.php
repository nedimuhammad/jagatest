<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>CRUD Application Category</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery-eeasyui/themes/default/easyui.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery-eeasyui/themes/icon.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery-eeasyui/themes/color.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery-eeasyui/demo/demo.css');?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-eeasyui/jquery.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-eeasyui/jquery.easyui.min.js');?>"></script>
</head>
<body>
	<h2> CRUD Category</h2>
	
	<table id="dg" title="Category" class="easyui-datagrid" style="width:1000px;height:300px"
			url="<?php echo site_url('category/json_datagrid_category'); ?>"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="id" width="10">ID</th>
				<th field="name_cat" width="50">Category</th> <!-- name dari tabel category -->
				<!--<th field="code" width="50">Code</th>
				<th field="name" width="70">Name</th>
				<th field="description" width="150">Description</th> -->
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newCategory()">New Category</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editCategory()">Edit Category</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyCategory()">Remove Category</a>
		<label>Name of Category:</label>
		<input name="name" id="name" style="line-height:20px;border:1px solid #ccc"></input>
		<a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()">Search</a>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:600px;height:400px;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Product Information</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>Category:</label>
				<input name="name_cat" class="easyui-textbox" required="true">
			</div>
			<!--<div class="fitem">
				<label>Code:</label>
				<input name="code" class="easyui-textbox" required="true">
			
			<div class="fitem">
				<label>Name:</label>
				<input name="name_cat" class="easyui-textbox" required="true">
			</div>
			<!--<div class="fitem">
				<label>Description:</label>
				<input name="description" class="easyui-textbox" style="width:100%;height:60px" data-options="multiline:true">
			</div> -->
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="save()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
	</div>
	<script type="text/javascript">
		var url;
		function newCategory(){
			$('#dlg').dialog('open').dialog('setTitle','New Category');
			$('#fm').form('clear');
			url = '<?php echo site_url('category/json_add_category');?>';
		}
		function editCategory(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Edit Category');
				$('#fm').form('load',row);
				url = '<?php echo site_url('category/json_edit_category/'); ?>'+row.id;
				// url = '<?php echo site_url('product/json_edit_product');?>?id='+row.id;
			}
		}
		function save(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyCategory(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this Category?',function(r){
					if (r){
						$.post('<?php echo site_url('category/json_destroy_category');?>',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Eeeeeeeeeeeeeeeeeeeeeeeeeeeerror',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
		function doSearch(){
			$('#dg').datagrid('load',{
				name: $('#name').val()
			});
		}
	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
</body>
</html>