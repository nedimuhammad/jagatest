<?php

	class Product_model extends CI_Model {
		
		public function __construct()
        {
                $this->load->database();
        }
	
		public function get_product(){
			
			$result['total'] = $this->db->get('product')->num_rows();
			
			$name=$this->input->post('name');
			$this->db->select('product.id,product.category,product.code,product.name,product.description,category.name_cat')
			->join('category','category.id=product.category');
			$this->db->where("product.name LIKE '%$name%'");
			$row = $this->db->get('product')->result_array(); //agar hasil array

			$result=array_merge($result,array('rows'=>$row));
			return $result;
			
			// $this->db->from('product'); // cara menggunakan from d CI
			// $this->db->where("product.category=category.product");
			// $query = $this->db->get('product');

			// return $query->result_array();
		}
		
		public function new_product()
		{
			return $this->db->insert('product',array(
            'category'=>$this->input->post('category',true),
            'code'=>$this->input->post('code',true),
            'name'=>$this->input->post('name',true),
            'description'=>$this->input->post('description',true)
        ));
			
		}
		
		public function edit_product($id)
		{
			$this->db->where('id', $id);
			return $this->db->update('product',array(
			'category'=>$this->input->post('category',true),
			'code'=>$this->input->post('code',true),
			'name'=>$this->input->post('name',true),
			'description'=>$this->input->post('description',true)
			)
			);
		}
		public function destroy_product($id)
		{
			return $this->db->delete('product', array('id' => $id)); 
		}
		public function search_product($name)
		{
			$this->db->like('name','$name');
			$this->db->get('product');
			// $query=$this->db->get('product');
			// return $query->result();
			
		}
	}
?>