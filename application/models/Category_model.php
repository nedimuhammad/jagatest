<?php

	class Category_model extends CI_Model {
		
		public function __construct()
        {
                $this->load->database();
        }
	
		public function get_category(){
			
			$result['total'] = $this->db->get('category')->num_rows();
			
			$name=$this->input->post('name');
			// $this->db->select('id');
			// $this->db->select('product.id,product.category,product.code,product.name,product.description,category.name_cat')->join('category.id=product.category');
			$this->db->from('category'); // cara menggunakan from d CI
			$this->db->where("name_cat LIKE '%$name%'");
			$row = $this->db->get()->result_array(); //agar hasil array

			$result=array_merge($result,array('rows'=>$row));
			return $result;
			
			// $query = $this->db->get('product');

			// return $query->result_array();
		}
		
		public function new_category()
		{
			return $this->db->insert('category',array(
            'name_cat'=>$this->input->post('name_cat',true)
        ));
		}
		
		public function edit_category($id)
		{
			$this->db->where('id', $id);
			return $this->db->update('category',array(
			'name_cat'=>$this->input->post('name_cat',true)
			)
			);
		}
		public function destroy_category($id)
		{
			return $this->db->delete('category', array('id' => $id)); 
		}
		// public function search_product($name)
		// {
			// $this->db->like('name','$name');
			// $this->db->get('product');
			// $query=$this->db->get('product');
			// return $query->result();
			
		// }
		public function getlist_category(){
			return $this->db->get('category')->result_array();
		}
	}
?>