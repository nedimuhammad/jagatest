<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('product_model');
                $this->load->model('category_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
               // $data['product'] = $this->product_model->get_product();
			   $data['json_url']=site_url('product/json_datagrid_product'); 
				$this->load->view('product/index',$data);
        }
		
		public function json_datagrid_product()
		{
			 // $name = intval(addslashes($_POST['name']));
			$data = $this->product_model->get_product();
			echo json_encode($data);
			
		}
		
		public function json_add_product()
		{
			$this->product_model->new_product();
			echo json_encode(array());
			// $data_create = $this->product_model->new_product();
			// echo json_encode($data_create);
			
		}
		
		public function json_edit_product($id=null)
		{
			$this->product_model->edit_product($id);
			echo json_encode(array('success'=>true));	
		}		
		
		public function json_destroy_product()
		{
			 $id = intval(addslashes($_POST['id']));
			$this->product_model->destroy_product($id);
				echo json_encode(array('success'=>true));	
		}
        public function json_search_product($name=null)
        {
			
             $this->product_model->search_product($name);
			 echo json_encode(array());
        }
		public function json_list_category()
		{
			$data = $this->category_model->getlist_category();
			echo json_encode($data);
		}
}