<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('category_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
               // $data['product'] = $this->product_model->get_product();
			   $data['json_url']=site_url('category/json_datagrid_category'); 
				$this->load->view('category/index',$data);
        }
		
		public function json_datagrid_category()
		{
			 // $name = intval(addslashes($_POST['name']));
			$data = $this->category_model->get_category();
			echo json_encode($data);
			
		}
		
		public function json_add_category()
		{
			$this->category_model->new_category();
			echo json_encode(array());
			// $data_create = $this->product_model->new_product();
			// echo json_encode($data_create);
			
		}
		
		public function json_edit_category($id=null)
		{
			$this->category_model->edit_category($id);
			echo json_encode(array('success'=>true));	
		}		
		
		public function json_destroy_category()
		{
			 $id = intval(addslashes($_POST['id']));
			$this->category_model->destroy_category($id);
				echo json_encode(array('success'=>true));	
		}
        public function json_search_category($name=null)
        {
			
             $this->category_model->search_category($name);
			 echo json_encode(array());
			 //test
        }
		
}